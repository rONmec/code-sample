# Code-Sample

The two main resources in the API are functions and jobs.

# Functions

Functions are basically any operation or script that is required. THe concept is inspired form mimicking AWS lamda, but without any time or resource constraints. So any number of fuctions can be added. Like tiling a tif file, or generating point clouds etc which were discussed during the interview.

# Jobs

Jobs are the instances of the functions, which run inside a kubernetes cluster.