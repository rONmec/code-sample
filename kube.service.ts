import { Injectable, Logger } from '@nestjs/common';
import AWS = require('aws-sdk');
import k8s = require('@kubernetes/client-node');
import { ConfigService } from '../config/config.service';

const generate = require('nanoid/generate')
const region = 'us-east-1';
const jobNamespace = new ConfigService(`${process.env.NODE_ENV}.env`).get('NAMESPACE')

@Injectable()
export class KubeService {
	constructor(private readonly configService: ConfigService) {
		this.initializeK8();
	}

	k8sBatchApi: any;
	k8sCoreApi: any;

	initializeK8() {
		const kc = new k8s.KubeConfig();
		kc.loadFromCluster();
		this.k8sCoreApi = kc.makeApiClient(k8s.Core_v1Api);
		this.k8sBatchApi = kc.makeApiClient(k8s.Batch_v1Api);
	}

	async buildFunction(func): Promise<any> {
		try {
			let repository = { repositoryName: '', repositoryUri: '' };
			let version = '1';
			if (func.image && typeof (func.image.id) == 'string') {
				if (func.status == 'READY') {
					version = (func.image.buildVersion + 1).toString();
				} else {
					version = func.image.buildVersion.toString();
				}
				repository.repositoryUri = func.image.id;
				repository.repositoryName = func.image.repo;
			} else {
				repository = await createRepository(`${func.functionId}-${this.configService.get('ENV_NAME')}`, region);
			}
			let jobName = parseString(func.name) + '-' + 'build' + generate('1234567890abcdef', 10);
			let jobManifest = generateBuildManifest(jobName, func.source.resource, repository.repositoryUri, func.id, 'v' + version.toString(), this.configService.get('REDIS_ENDPOINT'));
			let job = await this.k8sBatchApi.createNamespacedJob(jobNamespace, jobManifest);
			let matchLabel = job.response.body.metadata.uid;
			return {
				name: job.body.metadata.name,
				namespace: jobNamespace,
				repositoryUri: repository.repositoryUri,
				repositoryName: repository.repositoryName,
				version: version,
				matchLabel: matchLabel,
				jobName: jobName
			};
		} catch (err) {
			throw new Error(err);
		}
	}

	async createJob(job, func): Promise<any> {
		try {
			let jobManifest = generateJobManifest(job, func, this.configService.get('REDIS_ENDPOINT'));
			let kubeJob = await this.k8sBatchApi.createNamespacedJob(jobNamespace, jobManifest);
			let matchLabel = kubeJob.response.body.metadata.uid;
			return {
				matchLabel: matchLabel
			};
		} catch (err) {
			throw new Error(err);
		}
	}

	async getJobStatus(jobName, jobNamespace): Promise<any> {
		try {
			let status = await this.k8sBatchApi.readNamespacedJobStatus(jobName, jobNamespace);
		} catch (err) {
			throw new Error(err);
		}
	}

	async deleteJob(job): Promise<any> {
		try {
			let podName = await this.getPodName(job.matchLabel)
			let res = await this.k8sBatchApi.deleteNamespacedJob(job.name, jobNamespace, {});
			await this.k8sCoreApi.deleteNamespacedPod(podName, jobNamespace, {});
			return res;
		} catch (err) {
			throw new Error(err);
		}
	}

	async getPodLogs(podName, containerName): Promise<any> {
		try {
			let logs = await this.k8sCoreApi.readNamespacedPodLog(podName, jobNamespace, containerName, false, 10485760, 'true');
			return logs;
		} catch (err) {
			throw new Error(err)
		}
	}

	async getPodName(matchLabel): Promise<any> {
		try {
			let podInfo = await this.k8sCoreApi.listNamespacedPod(jobNamespace, true, null, null, true, 'controller-uid=' + matchLabel);
			return podInfo.body.items[0].metadata.name;
		} catch (err) {
			throw new Error(err)
		}
	}

	async deleteRepo(repoInfo): Promise<any> {
		try {
			let ecr = new AWS.ECR({ region: region });
			let params = {
				force: true,
				repositoryName: repoInfo.repo
			};
			let repo = await ecr.deleteRepository(params).promise();
			return repo;
		} catch (err) {
			throw new Error(err);
		}
	}


};

