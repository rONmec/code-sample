import { Post, Controller, Req, Body, HttpStatus, Get, Param, Put, Delete, Query, Patch, OnModuleInit, Logger, HttpException, Session } from '@nestjs/common';
import { AppService } from './app.service';
import { KubeService } from './kube/kube.service';
import { ConfigService } from './config/config.service';
import { AuthContext } from './auth-context.decorator';
const Redis = require('ioredis');

@Controller('api/v2/')
export class AppController implements OnModuleInit {
	constructor(private readonly appService: AppService, private readonly kubeService: KubeService, private readonly configService: ConfigService) { }

	@Post('functions')
	async createFunction(@Body() body, @AuthContext() context) {
		try {
			return await this.appService.createFunction(body, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Get('functions/:id')
	async findOneFunction(@Param('id') id, @AuthContext() context) {
		try {
			return await this.appService.getFunction({ id: id }, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Get('functions')
	async findAllFunction(@Query() queryParams, @AuthContext() context) {
		try {
			return await this.appService.getAllFunctions(queryParams, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Put('functions/:id')
	async updateFunction(@Param('id') id, @Body() body, @AuthContext() context) {
		try {
			return await this.appService.updateFunction(id, body, context);
		} catch (err) {
			Logger.error(err)
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Delete('functions/:id')
	async removeFunction(@Param('id') id, @AuthContext() context) {
		try {
			let func = await this.appService.getFunction({ id: id }, context);
			if (func.image) {
				await this.kubeService.deleteRepo(func.image);
			}
			return await this.appService.deleteFunction({ id: id }, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Patch('functions/:id/build')
	async buildFunction(@Param('id') id, @AuthContext() context) {
		try {
			let func = await this.appService.getFunction({ id: id }, context);
			let job = await this.kubeService.buildFunction(func);
			let buildOutput = {
				status: 'BUILDING',
				image: {
					buildVersion: job.version,
					id: job.repositoryUri,
					repo: job.repositoryName,
					buildJobMatchLabel: job.matchLabel,
					buildJobName: job.jobName
				}
			}
			await this.appService.updateFunction(func.id, buildOutput, context);
			return buildOutput;
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}


	@Post('jobs')
	async createJob(@Body() body, @AuthContext() context) {
		try {
			let func = await this.appService.getFunction({ id: body.id }, context);
			let job = await this.appService.createJob(body, func, context);
			let kubeJob = await this.kubeService.createJob(job, func);
			let updatedJob = await this.appService.updateJob(job.id, { status: 'ACTIVE', matchLabel: kubeJob.matchLabel }, context);
			return {
				job: updatedJob,
				status: 'SUCCESS',
				msg: 'Job started'
			};
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Get('jobs/:id')
	async findOneJob(@Param('id') id, @AuthContext() context) {
		try {
			return await this.appService.getJob({ id: id }, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Get('jobs')
	async findAllJobs(@Query() queryParams, @AuthContext() context) {
		try {
			return await this.appService.getAllJobs(queryParams, context);
		} catch (err) {
			Logger.error(err)
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Put('jobs/:id')
	async updateJob(@Param('id') id, @Body() body, @AuthContext() context) {
		try {
			return await this.appService.updateJob(id, body, context);
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}

	@Patch('jobs/:id/abort')
	async abortJob(@Param('id') id, @AuthContext() context) {
		try {
			let job = await this.appService.getJob({ id: id }, context);
			let podName = await this.kubeService.getPodName(job.matchLabel)
			let logFileString = await this.kubeService.getPodLogs(podName, null)
			await this.kubeService.deleteJob(job);
			if (logFileString.response) {
				logFileString = logFileString.response.body
			}
			let logFilePath = this.configService.get('ENV_NAME') + '/jobs/' + id + '-' + new Date().toISOString() + '.log'
			await this.appService.pushLogs(logFilePath, logFileString)

			let logs = []
			if (job.logs) {
				logs = job.logs
			}
			logs.push(logFilePath)
			await this.appService.updateJob(id, { status: 'ABORTED', logs: logs }, context)
			await this.appService.pushMail(job, 'ABORTED');
			return {
				message: 'Job Termination Complete',
				job: job
			};
		} catch (err) {
			Logger.error(err);
			throw new HttpException({
				status: HttpStatus.BAD_REQUEST,
				error: err,
			}, 400);
		}
	}
	async messages(appService, kubeService, configService) {
		try {
			const redis = new Redis(6379, configService.get('REDIS_ENDPOINT'))
			redis.subscribe('jobs', 'progress', 'functions', function (err, count) {
				if (err) {
					Logger.error(err);
					throw new Error(err);
				}
			});
			redis.on('message', async (channel, message) => {
				message = JSON.parse(message);
				if (channel == 'jobs') {
					let job: any = await appService.getJob({ id: message.id })
					if (job.status != 'ABORTED') {
						let podName = await kubeService.getPodName(job.matchLabel)
						let logFileString = await kubeService.getPodLogs(podName, null)
						await kubeService.deleteJob(job)
						if (logFileString.response) {
							logFileString = logFileString.response.body
						}
						let logFilePath = configService.get('ENV_NAME') + '/jobs/' + message.id + '-' + new Date().toISOString() + '.log'
						await appService.pushLogs(logFilePath, logFileString)
						let logs = []
						if (job.logs) {
							logs = job.logs
						}
						logs.push(logFilePath)
						await appService.updateJob(message.id, { status: message.status, logs: logs }, { authorized: true, userId: 'system' })
						await this.appService.pushMail(job, message.status)
					}
				}
				if (channel == 'functions') {
					let func: any = await appService.getFunction({ id: message.id })
					let podName = await kubeService.getPodName(func.image.buildJobMatchLabel)
					let logFileString = await kubeService.getPodLogs(podName, 'kaniko')
					await kubeService.deleteJob({ matchLabel: func.image.buildJobMatchLabel, name: func.image.buildJobName })
					if (logFileString.response) {
						logFileString = logFileString.response.body
					}
					let logFilePath = configService.get('ENV_NAME') + '/functions/' + func.functionId + '-' + new Date().toISOString() + '.log'
					await appService.pushLogs(logFilePath, logFileString)
					let logs = []
					if (func.logs) {
						logs = func.logs
					}
					logs.push(logFilePath)
					await appService.updateFunction(message.id, { status: message.status, logs: logs }, { authorized: true, userId: 'system' })
					await this.appService.pushMail(func, message.status)
				}
			});
		} catch (err) {
			Logger.error(err);
			throw new Error(err);
		}
	}

	onModuleInit() {
		this.messages(this.appService, this.kubeService, this.configService);
	}

}

