import { Injectable, BadRequestException, Logger } from '@nestjs/common';
import { LighthouseSchema } from './lighthouse-schema';
import { DataMapper } from '@aws/dynamodb-data-mapper';
import DynamoDB = require('aws-sdk/clients/dynamodb');
import * as Joi from 'joi';
import AWS = require('aws-sdk');
import { ConfigService } from './config/config.service';

const client = new DynamoDB({ region: 'us-east-1' });

const mapper = new DataMapper({ client });

const s3 = new AWS.S3();

const ses = new AWS.SES({ region: 'us-east-1' });

const generate = require('nanoid/generate')

@Injectable()
export class AppService {
	constructor(private readonly configService: ConfigService) { }
	private createItem = async (data): Promise<any> => {
		try {
			data = Object.assign(new LighthouseSchema(), data);
			let result = await mapper.put(data)
			return result
		} catch (err) {
			throw new Error(err);
		}
	}

	private getItem = async (data): Promise<any> => {
		try {
			data = Object.assign(new LighthouseSchema(), data);
			return await mapper.get(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	private updateItem = async (data): Promise<any> => {
		try {
			data = Object.assign(new LighthouseSchema(), data);
			return await mapper.update(data, { onMissing: 'skip' });
		} catch (err) {
			throw new Error(err);
		}
	}

	private deleteItem = async (data): Promise<any> => {
		try {
			data = Object.assign(new LighthouseSchema(), data);
			return await mapper.delete(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	private queryItems = async (data): Promise<any> => {
		try {
			let options: any = {};
			if (data.limit) {
				options.limit = data.limit ? data.limit : 25;
				delete data.limit
			}
			if (data.startKey) {
				options.startKey = JSON.parse(data.startKey);
				delete data.startKey
			}
			if (data.functionId != null) {
				options.indexName = 'functionId-createdAt-index'
			}
			else if (data.type != null && data.status != null) {
				options.indexName = 'type-status-index'
			}
			else if (data.type != null) {
				options.indexName = 'type-createdAt-index'
			}
			else {
				throw new Error('Not enough parameters to perform a query');
			}
			let items = [];
			let iterator = mapper.query(LighthouseSchema, data, options).pages();
			for await (const record of iterator) {
				items = record
			};
			return {
				items: items,
				count: iterator.count,
				lastKey: iterator.lastEvaluatedKey
			}
		} catch (err) {
			throw new Error(err);
		}
	}
	private getFunctionSchema(operation) {
		const functionSchema = Joi.object().keys({
			id: Joi.string().default(),
			name: Joi.string(),
			description: Joi.string(),
			type: Joi.string(),
			status: Joi.string(),
			spec: Joi.any(),
			runtime: {
				exec: Joi.string(),
				args: Joi.array().items(Joi.string()),
				options: Joi.array().items(Joi.any()),
				flags: Joi.array().items(Joi.any()),
			},
			alerts: Joi.array().items(Joi.any()),
			source: {
				type: Joi.string().required(),
				resource: Joi.string().required()
			},
			image: {
				buildVersion: Joi.number().integer(),
				buildInfo: Joi.string(),
				id: Joi.string(),
				buildJobMatchLabel: Joi.string(),
				buildJobName: Joi.string(),
				repo: Joi.string()
			},
			systemSpec: {
				memory: Joi.number(),
				cpu: Joi.number().integer(),
				storage: Joi.number()
			},
			logs: Joi.array().items(Joi.string()),
			author: Joi.any(),
			createdBy: Joi.string(),
			modifiedBy: Joi.string()
		});
		if (operation === 'create') {
			return functionSchema.requiredKeys('name').append({
				type: Joi.string().default('FUNCTION'),
				status: Joi.string().default('QUEUED'),
			});
		}
		if (operation === 'update' || 'get' || 'delete') {
			return functionSchema.requiredKeys('id');
		}
		if (operation === 'build') {
			return functionSchema.requiredKeys('runtime.exec', 'runtime.args', 'source.type', 'source.resource');
		}
		if (!operation) {
			return functionSchema;
		};
	}

	private getJobSchema(operation) {
		const jobSchema = Joi.object().keys({
			id: Joi.string().default(),
			name: Joi.string(),
			functionId: Joi.string(),
			status: Joi.string(),
			spec: Joi.any(),
			cmd: Joi.array(),
			//podNames: Joi.array().items(Joi.string()),
			matchLabel: Joi.string(),
			alerts: Joi.array().items(Joi.any()),
			logs: Joi.array().items(Joi.string()),
			createdBy: Joi.string(),
			modifiedBy: Joi.string()
		});
		if (operation === 'create') {
			return jobSchema.requiredKeys('functionId').append({
				type: Joi.string().default('JOB'),
				status: Joi.string().default('QUEUED'),
			});
		}
		if (operation === 'update' || 'get') {
			return jobSchema.requiredKeys('id');
		}
		if (!operation) {
			return jobSchema;
		};
	}
	private parseFunctionName(str): string {
		return (str.replace(/\s+/g, '-') + '-' + generate('1234567890abcdef', 10)).toLowerCase();
	}
	private parseJobName(str): string {
		return str.replace(/\s+/g, '-').toLowerCase() + Date.now().toString()
	}
	async createFunction(data, context): Promise<any> {
		try {
			if (context.authorized) {
				data.createdBy = data.modifiedBy = context.userId
				data.author = `${context.payload.given_name} ${context.payload.family_name}`
			}
			data = await Joi.validate(data, this.getFunctionSchema('create'));
			data.functionId = this.parseFunctionName(data.name);
			return await this.createItem(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	async getFunction(data, context): Promise<any> {
		try {
			data = await Joi.validate(data, this.getFunctionSchema('get'))
			let response = await this.getItem(data);
			if (response.type != 'FUNCTION')
				throw new Error('Not a function!');
			return response;
		} catch (err) {
			throw new Error(err);
		}
	}

	async getAllFunctions(queryParams, context): Promise<any> {
		try {
			queryParams.type = 'FUNCTION';
			return await this.queryItems(queryParams);
		} catch (err) {
			throw new Error(err);
		}
	}

	async updateFunction(id, data, context): Promise<any> {
		try {
			data.id = id;
			if (context.authorized) {
				data.modifiedBy = context.userId
			}
			data = await Joi.validate(data, this.getFunctionSchema('update'));
			return await this.updateItem(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	async deleteFunction(data, context): Promise<any> {
		try {
			if (context.authorized) {
				data.modifiedBy = context.userId
			}
			data = await Joi.validate(data, this.getFunctionSchema('delete'));
			return await this.deleteItem(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	async createJob(data, func, context): Promise<any> {
		try {
			try {
				if (func.status != 'READY')
					throw new Error('Function not ready!');
				else {
					let job: any = {
						functionId: func.functionId,
						spec: data.cmd,
						name: this.parseJobName(func.name),
						alerts: func.alerts
					}

					if (context.authorized) {
						job.createdBy = job.modifiedBy = context.userId
					}
					job = await Joi.validate(job, this.getJobSchema('create'));
					return await this.createItem(job);
				}
			} catch (err) {
				throw new Error(err);
			}
		} catch (err) {
			throw new Error(err);
		}
	}

	async getJob(data, context): Promise<any> {
		try {
			data = await Joi.validate(data, this.getJobSchema('get'));
			let response = await this.getItem(data);
			if (response.type != 'JOB')
				throw new Error('Not a job!');
			return response;
		} catch (err) {
			throw new Error(err);
		}
	}

	async getAllJobs(queryParams, context): Promise<any> {
		try {
			queryParams.type = 'JOB';
			return await this.queryItems(queryParams);
		} catch (err) {
			throw new Error(err);
		}
	}

	async updateJob(id, data, context): Promise<any> {
		try {
			data.id = id;
			if (context.authorized) {
				data.modifiedBy = context.userId
			}
			data = await Joi.validate(data, this.getJobSchema('update'));
			return await this.updateItem(data);
		} catch (err) {
			throw new Error(err);
		}
	}

	async pushLogs(logFilePath, logFileString): Promise<any> {
		try {
			return await s3.putObject({
				Bucket: this.configService.get('BUCKET'),
				Key: logFilePath,
				Body: logFileString
			}).promise();

		}
		catch (err) {
			throw new Error(err);
		}
	}

	async pushMail(object, status): Promise<any> {
		try {
			let recipients = []
			let mailerResponse;
			if (object.alerts) {
				for (let alert of object.alerts) {
					if (alert.onStatus === status) {
						recipients.push(alert.sendErrorReportTo);
					}
				}
				if (recipients.length > 0) {
					let subject = 'Lighthouse Update - ' + object.type + ' ' + status
					let link = this.configService.get('CLIENT_URL') + object.type === 'JOB' ? 'jobs/' : 'functions/' + object.id
					let body = '<p>Hi</p><p>The ' + object.functionId + ' with name ' + object.name + ' has ended with status ' + status + '. Click <a href="' + link + '" target="_blank">here</a> to know more.</p> <p>Regards</p><p><STRONG>The Lamp at the Lighthouse<STRONG></p>'
					let params = {
						Destination: {
							ToAddresses: recipients
						},
						Message: {
							Body: {
								Html: {
									Charset: "UTF-8",
									Data: body
								}
							},
							Subject: {
								Charset: "UTF-8",
								Data: subject
							}
						},
						Source: "lighthouse@indshine.com",
					};
					mailerResponse = await ses.sendEmail(params).promise();
				}
			}
			return mailerResponse;
		}
		catch (err) {
			throw new Error(err);
		}
	}
}


